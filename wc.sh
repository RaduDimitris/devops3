#!/bin/bash
# set -x
# scriptul trebuie sa imite comanda WC din bash
# si anume daca primeste parametrul:
# -l sa numere toate liniile dintr-un fisier
# -w sa numere toate cuvintele dintr-un fisier
# -c sa numere toate characterele dintr-un fisier cu exceptia spatiului
# exemplu comanda: ./wc.sh -l file.txt
# Vom folosi un case ca sa verificam primul argument si asociat fiecarui caz vom avea o functie

ARG1=$1
ARG2=$2

count_lines () {
    if [ -f $ARG2 ]; then
        echo "Reading lines..."
        count=0
        while IFS= read -r line
        do
            count=$(( $count + 1 ))
        done < $ARG2
        echo $ARG2 ":" $count
    else
        echo "The file does not exists"
    fi       
}

count_words () {
    if [ -f $ARG2 ]; then
        echo "Counting words..."
        count=0
        while IFS= read -r line
        do
            for word in $line; do
                count=$(( $count + 1 ))
            done
        done < $ARG2
        echo $ARG2 ":" $count
    else
        echo "The file does not exists"
    fi
}

count_chars () {
    if [ -f $ARG2 ]; then
        echo "Counting characters..."
        count=$(cat $ARG2)
	let count=${#count}+1
	#while IFS= read -r line
        #do
            # for word in $line; do
                # If I want to see how many chars has each word
                # echo $word "has" ${#word} "chars"
                # echo $line
                # count=$(( $count + ${#line} ))
                # count=$(( $count + ${#word} ))
                # echo $count
            # done
        #done < $ARG2
        echo $ARG2 ":" $count
    else
        echo "The file does not exists"
    fi
}

case $ARG1 in
    -l)
        if [ ${#ARG2} -eq 0 ]; then
            echo "Argument missing"
            exit
        fi
        count_lines
        ;;
    -w)
        if [ ${#ARG2} -eq 0 ]; then
            echo "Argument missing"
            exit
        fi
        count_words
        ;;
    -c)
        if [ ${#ARG2} -eq 0 ]; then
            echo "Argument missing"
            exit
        fi
        count_chars
        ;;
    *)
        echo "Wrong argument"
        ;;
esac
